# Summary

[Introduction](./00-introduction.md)

- [Prerequisites](./05-00-prerequisites.md)

- [Installing the Management Cluster](./10-00-management-cluster.md)

    - [Hardware Requirements](./10-10-hardware-requirements.md)
    - [Installing the Management Kubernetes](./10-20-management-k8s.md)
    - [Installing Additional Services](./10-30-svcs.md)
    - [Configuring the Services and Installing YAOOK Metal Controller](./10-60-metal-controller.md)

- [Creating a Cluster](./20-00-creating-a-cluster.md)

    - [Inventorizing Nodes](./20-10-adding-nodes.md)
    - [Deploying Nodes](./20-20-deploying.md)

- [Installing OpenStack](./30-installing-openstack.md)

---

- [Migrating an existing OpenStack Cluster](./40-00-migrating-openstack.md)

    - [Migrating Glance](./40-10-migrating-glance.md)
    - [Migrating Keystone](./40-20-migrating-keystone.md)
    - [Migrating Cinder](./40-30-migrating-cinder.md)
    - [Migrating Nova Controlplane](./40-40-migrating-nova-controlplane.md)
    - [Migrating Neutron Controlplane](./40-50-migrating-neutron-controlplane.md)

---

- [Operations](./50-operations.md)

  - [Database Operations](./50-00-database-operations.md)

    - [Backup Restore](./50-00-00-database-backup-restore.md)

    - [Galera Recovery](./50-00-10-galera-recovery.md)
