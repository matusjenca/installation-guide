# Galera Recovery

If your galera cluster breaks because of bootstrapping issues or missing primaries you can follow this guide to fix it again.
This guide assumes you are running an actual galera cluster. It is probably not applicable for single nodes.

**WARNING** During this guide you will stop the cluster.
If your cluster is still serving requests the measures taken here might not be necessary.

## Preparations

Install yaookctl from https://gitlab.com/yaook/yaookctl by cloning the repo locally or by using `pip install git+https://gitlab.com/yaook/yaookctl.git`.

## Getting the cluster to a known state

To ensure we have a well known and state of the cluster the cluster must first be stopped completely.
```
yaookctl -n yaook pause mysqlservices <your-MysqlService-name-here>
```

After that scale the `statefulset` of the galera cluster to 0.
From now on we assume the statefulset of the cluster is named `mybrokengalera-statefulset-db`.

Wait for all pods of the statefulset to terminate. This might take a few minutes (5-10) per pod.
Do not kill the pods as this will make recovery harder.


## Determining the state of the individual galera nodes

The next step is to determine the replication status of the individual galera nodes.
To do this run the following

```
$ yaookctl galera find-wsrep-positions <your-MysqlService-name-here>
```

This will give you something like the following output:
```
+--------------------------+--------------------------------------+--------+-------+------------+-------+
| PVC Name                 | UUID                                 | SeqNo  | Safe? | Recovered? | Best  |
+--------------------------+--------------------------------------+--------+-------+------------+-------+
| data-keystone-lx9vg-db-0 | d8db3e8c-408b-11ed-92ac-c61d317a972a | 146820 | True  | False      | True  |
| data-keystone-lx9vg-db-1 | d8db3e8c-408b-11ed-92ac-c61d317a972a | 146819 | False | True       | False |
| data-keystone-lx9vg-db-2 | d8db3e8c-408b-11ed-92ac-c61d317a972a | 146806 | False | False      | False |
+--------------------------+--------------------------------------+--------+-------+------------+-------+
```

Decide for the pvc from which to start from.
Unless you have specific reasons to choose something else this should always be the one where the column `Best` is `True`.
From this row take the PVC Name and take the index of it. So in the case above the index is `0`.

## Restarting from a specific galera node

Run the following command:

```
yaookctl galera force-bootstrap <your-MysqlService-name-here> <your-index>
```

This will startup the necessary database pods and modify them so that they recover automatically if needed.

Wait for the statefulset to get to its target size and determine if the node we are bootstrapping from is working.
Since the probes are removed, you will need to manually check the logs until you have the line `WSREP: Shifting JOINED -> SYNCED`.

Update the statefulset again and remove the above created if condition (you need to also move the exec line back).
Additionally set the following (you might need to delete other contents in `updateStrategy`):
```yaml
spec:
    updateStrategy:
        type: OnDelete
```

Scale the cluster to its full target size one-by-one.
After each scale up, check that the new pod has correctly synchronized with the cluster.

Now, one-by-one, delete all pods lower than your determined index.
They will now also start galera and should join the cluster.
Wait again until each member is synchronized.

After that is done, remove the `updateStrategy` statement from the statefulset.

**WARNING** ensure that `updateStrategy` is actually removed from the statefulset. Failure to do so will cause the cluster to break later.

## Cleanup

```
yaookctl -n yaook unpause mysqlservices <your-MysqlService-name-here>
```

**NOTE** This will cause a rolling restart of the galera cluster.
