# Installing Additional Services

The following additional services are needed to use the YAOOK metal controller as-is to provision clusters[^extensibility]:

- [HashiCorp Vault](https://www.vaultproject.io/): This is used to provide an identity to nodes and to store secrets shared between the cluster nodes.

- [NetBox](https://github.com/netbox-community/netbox/): Provides the network model and the desired state of the nodes.

- [OpenStack Ironic](https://docs.openstack.org/ironic/): Manages the hardware and controls the deployment process.

[^extensibility]: The only backend implemented as of writing requires Vault and NetBox. It is possible to implement different backends for the metal-controller which use different sources of truth and different places to store secrets.

It is not strictly necessary to install these components within the same Kubernetes cluster as the metal controller. The following guide is good enough to get a basic setup running, but it is nowhere near production ready.

## Using the management-cluster Ansible playbook

The [management-cluster](https://gitlab.com/yaook/incubator/management-cluster) ansible playbook is designed to run against a working Kubernetes cluster and to provision additional services for Yaook.

It can install NetBox and Vault. To use it, a configuration file needs to be created. The configuration is stored as ansible variables and then passed to the `ansible-playbook` call later on. The options will be documented for the individual services below and we will assume that you use `config.yaml` as configuration file.

### Installing HashiCorp Vault using the management-cluster playbook

**Note:** The Vault setup in the management-cluster playbook is **not** ready for productive use. It prints important keys in plaintext to stdout, it has no backups configured and it is not set up highly-available.

Work [is currently in progress](https://gitlab.com/yaook/k8s/-/merge_requests/598) which adds support for a proper HA Vault setup to yaook/k8s itself.

#### Configuration

```yaml
yaook_vault_dnsname: <FQDN>
```

##### `yaook_vault_dnsname`

This must be the "public" DNS name (outside of the cluster) of the Vault service in fully-qualified form. This will be included in the certificate request for the Vault service as well as in the Ingress definition.

**Note:** This must be resolvable and accessible from any **deployed** node.

#### Installation

You need to have a `KUBECONFIG` loaded which points at the Kubernetes cluster you want to install vault in. It will be installed into the `yaook` namespace by default, but that can be customized with the `yaook_vault_namespace` option **before the first installation**.

```console
$ ansible-playbook --diff -t yaook-vault -i inventory/default.yaml -e @config.yaml install.yaml
```

**NOTE:** The first run (and only the first run) will print the secrets necessary to continue operating the Vault (unseal keys and a root token). Store these secrets in a safe place.

**NOTE:** For safety reasons, the Vault StatefulSet will never update its pods, unless you explicitly delete the Pod. This is because you will have to manually unseal the vault afterward, using the unseal keys you obtained during first installation.

### Installing NetBox using the management-cluster playbook

**Note:** The NetBox setup in the management-cluster playbook is **not** ready for productive use. Its database is configured using an off-the-shelf helm chart and does not include any backup mechanism.

#### Configuration

```yaml
yaook_netbox_dnsname: <FQDN>
yaook_netbox_superuser_password: <password>
yaook_netbox_superuser_api_token: <secret token>
yaook_netbox_postgresql_password: <password>
```

##### `yaook_netbox_dnsname`

This must be the "public" DNS name (outside of the cluster) of the Vault service in fully-qualified form. This will be included in the certificate request for the Vault service as well as in the Ingress definition.

##### `yaook_netbox_superuser_password`

This is the password of the automatically-created superuser (`admin`). Keep it safe.

##### `yaook_netbox_superuser_api_token`

This is the API token which will be automatically created by the helm chart. Unfortunately, there is no way to turn this off and this token is as powerful as the password, so choose it securely.

##### `yaook_netbox_postgresql_password`

This is the password used for the Netbox user in the postgresql database.

#### Installation

You need to have a `KUBECONFIG` loaded which points at the Kubernetes cluster you want to install vault in. It will be installed into the `yaook` namespace by default, but that can be customized with the `yaook_netbox_namespace` option **before the first installation**.

```console
$ ansible-playbook --diff -t yaook-netbox -i inventory/default.yaml -e @config.yaml install.yaml
```

## Installing OpenStack Keystone

Install keystone as usual (detailed guide TBD), except that you install it into the `yaook` namespace.

## Installing OpenStack Ironic

Install the infra-ironic-operator into the cluster into the `yaook` namespace. You should then be able to fill in the gaps in the following template to get your InfrastructureIronicDeployment up and running:

```yaml
---
apiVersion: yaook.cloud/v1
kind: InfrastructureIronicDeployment
metadata:
    name: infra-ironic
    namespace: yaook
spec:
    ingressAddress: <INGRESS-IP>
    api:
        replicas: 1
        ingress:
            fqdn: <IRONIC FQDN>
            port: 32080
    inspectorApi:
        replicas: 1
        ingress:
            fqdn: <INSPECTOR FQDN>
            port: 32080
    database:
        ironic:
            replicas: 1
            storageClassName: local-storage
            storageSize: 8Gi
            proxy: {}
            backup:
                schedule: "0 * * * *"
        inspector:
            replicas: 1
            storageClassName: local-storage
            storageSize: 8Gi
            proxy: {}
            backup:
                schedule: "0 * * * *"
    imageServer:
        ingress:
            fqdn: <IMAGE SERVER FQDN>
            port: 32080
        storageSize: 8Gi
        storageClassName: local-storage
    targetRelease: train
    issuerRef:
        name: ca-issuer
    pxe:
        listenNetwork: <CIDR OF KUBERNETES-NODES>
        dhcp:
            - dhcpRange: <DHCP RANGE START>,<DHCP RANGE END>,<SUBNETMASK>
              defaultGateway: <DEFAULT GATEWAY of this scope>
    dnsmasq:
        storageClassName: local-storage
    keystoneRef:
        name: keystone
```

Here, the following variables need extra thought:

- `spec.api.ingress.fqdn`: The "public" DNS name (outside of the k8s cluster) of the Ironic API. This does not necessarily need to exist in any real zone, as the dnsmasq deployed alongside will offer it to all nodes during the PXE stage automatically.

- `spec.api.ingressAddress`: Set to the IP the ingress is listening on. (necessary for dnsmasq entries, can not be automatically obtained if the ingress uses a service with the type NodePort - therefore it needs to be configured)

- `spec.api.inspectorApi.fqdn`: The "public" DNS name (outside of the k8s cluster) of the Ironic Inspector API. This does not necessarily need to exist in any real zone, as the dnsmasq deployed alongside will offer it to all nodes during the PXE stage automatically.

- `spec.api.imageServer.fqdn`: The "public" DNS name (outside of the k8s cluster) of the Image Server. This does not necessarily need to exist in any real zone, as the dnsmasq deployed alongside will offer it to all nodes during the PXE stage automatically.

- `spec.api.imageServer.pvcAccessmode`: The default pvc for the imageServer will be deployed with the accessMode RWO (Read-Write-Once). Due to the current setup some pods are sharing this created pvc. If all pods are always running on the same kubernetes-node RWO works fine. If you have a distributed / redundant cluster - pods on different worker can not mount this pvc and will fail in startup. So if the cluster contains of more than 1 kubernetes-node and your storage backend supports RWX consider setting this value to "ReadWriteMany".

- `spec.pxe.listenNetwork`: Set to the CIDR the kubernetes-nodes are placed in. This will then determine the correct IP to set in the dnsmasq (during startup) for the deriving listeners of dnsmasq. This will allow HA for dnsmasq but only for kubernetes-nodes within the same CIDR.
(Example: ListenNetwork: 172.24.116.128/26 > during startup of DNSMASQ the container checks all available ip-addresses on the kubernetes-node and uses the one matching the provided CIDR)
**Consideration for dhcp-relay**: if there is a need to use dhcp-relay you need to use all possible ips, of kubernetes-nodes dnsmasq can run on, as relay-targets.

- `spec.pxe.dhcp`: A list containing all dhcpRanges necessary. An entry needs to contain a dhcpRange, defaultGateway is optional per list-entry

- `spec.pxe.dhcp.dhcpRange`: The start and end IP address of the range from which dnsmasq will hand out temporary IP addresses during node discovery and provisioning. Can optionally also contain the subnet mask which is recommended if using a per scope defaultGateway

- `spec.pxe.dhcp.defaultGateway`: Necessary if using dhcp-relay, otherwise dhcp ips can never reach the ironic infrastructure. Value is optional per dhcpRange.

The ports of the ingress blocks also need to be set to the port of the ingress controller used.

### Uploading images

The Ironic installation does not come with any images and it is required that you manually download and place images for both the Ironic Python Agent as well as your deployed nodes.

Pre-built CentOS(!) images are available here: https://gitlab.com/yaook/disk-images/-/packages/

- The `ipa/ipa.initramfs` and `ipa/ipa.kernel` should be placed in `/usr/share/nginx/html` in the imgserv pod.
- The `centos8/centos8.qcow2` should be placed in `/usr/share/nginx/html` in the imgserv pod. Take an `md5sum` of it to hand it to the metal controller later.

**Note:** The package list is a bit chaotic. Make sure to use images built off the master branch.

You can use your own operating system image, though the deploy process assumes a cloud-init capable CentOS 8.
