# Migrating Neutron Controlplane

The following guide will show you how to migrate the neutron controlplane to YAOOK with minimal downtime.

This guide will not handle anything related to compute or router/gateway nodes.
They are handled in a separate guide.

## Downtime Impact

When following this guide you will have a downtime for changes (server creates/deleted, volume attachments) on the nova api depending on the size of your environment.
The time needed for the migration extremely depends on the amount of nodes, networks and host in them. For our 250 nodes we needed roughly 12 hours (when restarting 30 agents in parallel).
Existing servers should work just fine during the whole migration.

Note: the following procedure produced a unexpected data-plane outage in our environment.
See the section about potential improvements to hopefully avoid them (although these are just ideas and have not been tested).

## Requirements

Please ensure the following before following this guide:
* Ensure you have a ready YAOOK setup with operators and their dependencies up and running
* Ensure you have a `KeystoneDeployment` or `ExternalKeystoneDeployment` ready
* Ensure you have at least 1 IPs available for services of type `LoadBalancer`

## Preparations

### Preparations of dependent services

Services like nova (including compute nodes), heat or octavia rely on the neutron endpoint for their operations.
As the internal endpoint of neutron will be inside the YAOOK Kubernetes cluster it will no longer be reachable from the outside.
Therefor you might need to change the dependent services to use the `public` endpoint during service discovery.

If you can not change this before the migration, or if you use a static url for neutron, you can also do the change during the migration without issues.
This guide will point out when you should change it.

### Prepare a migration policy setting

During the migration you will need to prevent writes to neutron.
The easiest way to do this is to update the policy of the service and require admin permissions for all changes.
As the policy differs for each deployment please already prepare such a policy in advance.

### Prepare NeutronDeployment

Prepare a `NeutronDeployment` resource with the options you want to have set.
You can find documentation for the resource [here](https://docs.yaook.cloud/devel/examples/neutron.html).
You can completely ignore all the agent sections for now (`l2`, `dhcp`, `l3` and `bgp`).

## Migration

A part time of the migration can already be prepared without a downtime.
The downtime start will be explicitly mentioned below.

### Installation of Neutron

Installing a parallel neutron is already possible if you are careful that the existing keystone endpoint is not overwritten.

1. Stop the keystone-resource-operator by scaling its deployment to 0 and wait for it to terminate
2. Apply the `NeutronDeployment` you created above and wait for it to depend on the keystone user
3. Stop the neutron-operator by scaling its deployment to 0 and wait for it to terminate
4. Delete the neutron and placement endpoint in the `KeystoneEndpoint` resource
5. Start the keystone-resource-operator by scaling its deployment to 1
6. Wait for the neutron `KeystoneUser` to reconcile successfully
7. Stop the keystone-resource-operator by scaling its deployment to 0 and wait for it to terminate
8. Start the neutron-operator by scaling its deployment to 1
9. Wait for the `NeutronDeployment` to reconcile successfully
10. Update the `NeutronDeployment` policy setting to have your migration policy from above included
11. Update the `NeutronDeployment` setting `[DEFAULT] agent_down_time` to a value that should outlast the whole migration significantly (a week or so).
12. Wait for the reconcile to complete

### Making Neutron services available externally

The neutron-compute services and the neutron-metadata-agent need to be able to connect to internal neutron services.
These services are not exposed per default as for a yaook-only cluster they are not needed.
Until all of the neutron agents are migrated to yaook you will need to keep this Loadbalancer available.

1. Find the name of the `AmqpServer` of neutron, it should start with `neutron-`
2. Create the following 3 services inside your kubernetes cluster
```yaml
---
apiVersion: v1
kind: Service
metadata:
  name: neutron-mq-external
  namespace: yaook
spec:
  selector:
    state.yaook.cloud/component: amqpserver
    state.yaook.cloud/parent-group: infra.yaook.cloud
    state.yaook.cloud/parent-name: <put-the-neutron-cell1-amqpserver-name-here>
    state.yaook.cloud/parent-plural: amqpservers
    state.yaook.cloud/parent-version: v1
  type: LoadBalancer
  #loadBalancerIP: 192.168.0.14 # optional, you can also let this be autoassigned
  ports:
  - name: amqps
    port: 5671
    protocol: TCP
    targetPort: 5671
---
```

### Migration of Neutron

Starting here these steps will have the above described Downtime.
Please plan accordingly.

1. Update the neutron policy on your existing neutron environment to the migration policy defined above. This will prevent further modifications and therefor we can leave the api running.
2. Restart the neutron api services afterwards
3. Export the neutron database using `mysqldump`
4. Copy the dump to the first database container in k8s using "kubectl cp ..."
5. Get the neutron db admin user pw from the secret "neutron-**-db-creds" and the key "mariadb-root-password" in there
6. open a shell on the first neutron database pod in the "mariadb-galera" container
7. Load the db dump again using "mysql -u yaook-sys-maint neutron -p < neutron.dump" and enter the passwort when prompted
8.  If you now `curl` the new neutron ingress endpoint you should get a valid response
    - Get yourself a keystone token using `openstack token issue` and save the id
    - in a new shell without ANY OpenStack environment variables set, set the following
        - `OS_TOKEN` to the value of the token from above
        - `OS_ENDPOINT` to the url of neutron including the version (e.g. `https://neutron.example.com/v2/`)
    - now a `openstack network list` should return the expected result
9.  Start the keystone-resource-operator by scaling its deployment to 1
10. Wait for the "KeystoneEndpoint" resource to reconcile correctly
11. At this point the yaook `neutron` will be active for api requests.
12. Restart nova-api and placement pods one-by-one. They seem to cache the old neutron endpoint

### Migration of neutron agents

Do the following for each of the neutron agents.
You can run it on a few agents in parallel, but ensure you do not overload your neutron-server with requests.

1. Set static host entries on the node pointing to the neutron rabbitmq (the hostname in the hosts file MUST match the name of the service in k8s)
2. Add the CA certificate of certmanager to the system trusted certificates
3. Update the neutron agent configuration to use the new rabbitmq
4. Restart the neutron agent
5. Wait for the neutron agent to come up completely
   1. For `neutron-openvswitch-agents` this means they complete a iteration at least every 10 seconds
   2. For `neutron-l3-agents` this means they stop logging about adding routers
   3. For `neutron-dhcp-agents` this means they stop logging about adding dhcp services
   4. For `neutron-metadata-agent` this is directly done after a restart (if it does not log errors)

### Cleaning up

At this point nobody should be using the old neutron anymore.
You can validate that on the old neutron api logs.
If you still see requests you should clean them up now.

Afterwards:
- Stop the old neutron api
- revert your policy to whatever your original setting is

The migration should now be finished.
You can now also clean up everything related to the old neutron service.

You will need to keep the `LoadBalancer` services in kubernetes until you migrated all nodes using them

## Issues and potential improvements

At the point of restarting the neutron-l3-agents we restarted 2-3 of them in parallel.
For some reason this caused our keepalived's to start flapping which caused a partial networking outage.
This was made worse by the fact that not all neutron-openvswitch-agents where already up and running, which meant they could not update flows to point to the new location of a router.

To mitigate this issue you could already do the restarting of all neutron agents well in advance of the whole migration.
This could be accomplished in the following way:
1. Create the above mentioned service of type `LoadBalancer` already before starting this migration
2. Manually create an kubernetes `Endpoint` resource to point to your existing rabbitmq you are using for neutron at the moment
3. Validate that by connecting to the Loadbalancer ip address you actually reach your existing rabbitmq
4. Configure all of your neutron agents to use the new loadbalancer
5. After this is done start the migration process (you will need to ensure that the existing rabbitmq user works on the yaook rabbit cluster)

The benefit of this order is that you can change the configuration while being sure that all other neutron agents are running fine and catching updates.
This should eliminate any issue with updated not reaching the required nodes and causing network outages.
Also you do not have time pressure for this change since it is completely invisible to the openstack users.

If you look at it from the perspective of a neutron agent the  migration will then look like a short rabbitmq disconnect (which it should handle quite well).
