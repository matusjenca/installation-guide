# Hardware Requirements

These requirements are for *the management cluster* only.

The management cluster will be responsible for managing the life-cycle of your OpenStack hypervisor and control plane nodes. As such, it needs enough resources to enumerate those nodes and handle their requests during PXE deployment. It also needs to have a corresponding network setup in order to be albe to deploy nodes.

## Computing Hardware Requirements

### Minimum Requirements

For a minimal development or proof-of-concept setup, a single machine with 32 GiB of RAM and a dual core x86 processor should do the trick. You can probably get away with even less.

### Recommended Setup

For a productive setup, we recommend to have at least three nodes with 32 GiB of RAM and a 8-core CPU each. If you intend to deploy a significant amount of nodes in parallel, you'll also need sufficient bandwidth for those nodes to pull their OS images simultaneously.

## Additional Requirements

The management cluster nodes need access to the IPMI / BMC interfaces of the nodes they are supposed to deploy. In addition, they need layer 2 connectivity (or a DHCP relay) to a PXE-enabled network interface on those nodes.
