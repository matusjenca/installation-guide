# Migrating an existing OpenStack Cluster

If you are already running an OpenStack Cluster outside of YAOOK and want to migrate it then you can follow the steps in this chapter.
Since each OpenStack environment is different from each other you will need to adopt the procedures for your specific case.
